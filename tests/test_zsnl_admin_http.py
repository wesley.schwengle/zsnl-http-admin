# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from pyramid.httpexceptions import HTTPBadRequest, HTTPForbidden, HTTPNotFound
from unittest import mock
from uuid import uuid4
from zsnl_admin_http.views import views
from zsnl_domains.admin.catalog.entities import (
    CaseTypeVersion,
    Folder,
    FolderEntry,
)
from zsnl_domains.admin.catalog.entities.attribute import Attribute


class MockRequest:
    def __init__(
        self,
        cookies,
        sessions,
        params,
        query_instances,
        command_instances=None,
        current_route=None,
    ):
        self.cookies = cookies
        self.sessions = sessions
        self.params = params
        self.query_instances = query_instances
        self.command_instances = command_instances
        self.authorization = None

        if current_route is None:
            self.current_route = "mock_current_route"
        else:
            self.current_route = current_route

    @property
    def json_body(self):
        return self.body

    @property
    def session_id(self):
        return self.cookies["zaaksysteem_session"]

    @property
    def response(self):
        return mock.MagicMock()

    def retrieve_session(self):
        return self.sessions[self.session_id]

    def get_query_instance(self, domain, user_uuid):
        return self.query_instances[domain]

    def get_command_instance(self, domain, user_uuid):
        return self.command_instances[domain]

    def current_route_path(self):
        return self.current_route

    def route_path(self, name, _query=None):
        route = self.current_route.split("?")[0]
        print(_query)
        if _query:
            route = f"{route}?folder_id={_query['folder_id']}"
        return route


class TestViews:
    """Test view methods for admin http service"""

    def test_get_folder_contents_root_folder(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )
        request.query_instances[
            "zsnl_domains.admin.catalog"
        ].get_folder_contents.return_value = [
            FolderEntry(
                id=123,
                uuid="d234a001-d664-4c91-9048-6bbe78330a3c",
                name="Test folder entry",
                entry_type="case_type",
                active=True,
            ),
            FolderEntry(
                id=1235,
                uuid="34cdcba6-b8e0-4cbc-8800-fb149ebdf949",
                name="Test folder entry 2",
                entry_type="folder",
                active=True,
            ),
        ]

        contents = views.get_folder_contents(request)
        assert contents == {
            "data": [
                {
                    "type": "folder_entry",
                    "id": "d234a001-d664-4c91-9048-6bbe78330a3c",
                    "attributes": {
                        "type": "case_type",
                        "name": "Test folder entry",
                        "active": True,
                    },
                    "links": {
                        "self": "/api/v2/admin/catalog/get_entry_detail?type=case_type&item_id=d234a001-d664-4c91-9048-6bbe78330a3c"
                    },
                },
                {
                    "type": "folder_entry",
                    "id": "34cdcba6-b8e0-4cbc-8800-fb149ebdf949",
                    "attributes": {
                        "type": "folder",
                        "name": "Test folder entry 2",
                        "active": True,
                    },
                    "links": {
                        "self": "/api/v2/admin/catalog/get_entry_detail?type=folder&item_id=34cdcba6-b8e0-4cbc-8800-fb149ebdf949"
                    },
                },
            ],
            "links": {
                "parent": None,
                "grandparent": None,
                "self": {
                    "href": "mock_current_route",
                    "name": None,
                    "id": None,
                },
            },
        }

    def test_search_catalog(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )
        request.query_instances[
            "zsnl_domains.admin.catalog"
        ].search_catalog.return_value = [
            FolderEntry(
                id=123,
                uuid="d234a001-d664-4c91-9048-6bbe78330a3c",
                name="Test folder entry",
                entry_type="case_type",
                active=True,
            ),
            FolderEntry(
                id=1235,
                uuid="34cdcba6-b8e0-4cbc-8800-fb149ebdf949",
                name="Test folder entry 2",
                entry_type="folder",
                active=True,
            ),
        ]

        contents = views.search_catalog(request)
        assert contents == {
            "data": [
                {
                    "type": "folder_entry",
                    "id": "d234a001-d664-4c91-9048-6bbe78330a3c",
                    "attributes": {
                        "type": "case_type",
                        "name": "Test folder entry",
                        "active": True,
                    },
                    "links": {
                        "self": "/api/v2/admin/catalog/get_entry_detail?type=case_type&item_id=d234a001-d664-4c91-9048-6bbe78330a3c"
                    },
                },
                {
                    "type": "folder_entry",
                    "id": "34cdcba6-b8e0-4cbc-8800-fb149ebdf949",
                    "attributes": {
                        "type": "folder",
                        "name": "Test folder entry 2",
                        "active": True,
                    },
                    "links": {
                        "self": "/api/v2/admin/catalog/get_entry_detail?type=folder&item_id=34cdcba6-b8e0-4cbc-8800-fb149ebdf949"
                    },
                },
            ],
            "links": {"self": {"href": "mock_current_route"}},
        }

    def test_get_folder_contents_root_folder_child(self):
        mock_cqrs = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={
                "folder_id": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                "page": 1,
                "page_size": 50,
            },
            query_instances={"zsnl_domains.admin.catalog": mock_cqrs},
            current_route="/api/v2/admin/catalog/get_folder_contents?folder_id=f60b8a3d-5496-463d-bd15-5180bde7cc34",
        )

        mock_cqrs.get_folder_contents.return_value = [
            FolderEntry(
                id=23345,
                uuid="d234a001-d664-4c91-9048-6bbe78330a3c",
                name="Test folder entry",
                entry_type="case_type",
                active=True,
            )
        ]
        mock_cqrs.get_folder_details.return_value = Folder(
            id=1234,
            uuid="f60b8a3d-5496-463d-bd15-5180bde7cc34",
            name="child1",
            parent_uuid=None,
            parent_name=None,
            last_modified="2019-02-12",
        )

        contents = views.get_folder_contents(request)

        assert contents["links"] == {
            "grandparent": None,
            "parent": {
                "href": "/api/v2/admin/catalog/get_folder_contents",
                "name": None,
                "id": None,
            },
            "self": {
                "href": "/api/v2/admin/catalog/get_folder_contents?folder_id=f60b8a3d-5496-463d-bd15-5180bde7cc34",
                "name": "child1",
                "id": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
            },
        }

    def test_get_folder_contents_child_folder_with_child(self):
        mock_cqrs = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"folder_id": "f60b8a3d-5496-463d-bd15-5180bde7cc34"},
            query_instances={"zsnl_domains.admin.catalog": mock_cqrs},
            current_route="/api/v2/admin/catalog/get_folder_contents?folder_id=f60b8a3d-5496-463d-bd15-5180bde7cc34",
        )

        mock_cqrs.get_folder_contents.return_value = [
            FolderEntry(
                id=1234,
                uuid="d234a001-d664-4c91-9048-6bbe78330a3c",
                name="Test folder entry",
                entry_type="case_type",
                active=True,
            )
        ]

        def folder_details_side_effect(folder_id):
            if folder_id == "8a22f984-351e-11e9-8df4-afc0e680fd64":
                return Folder(
                    id=1234,
                    uuid="8a22f984-351e-11e9-8df4-afc0e680fd64",
                    name="child3",
                    parent_uuid="f60b8a3d-5496-463d-bd15-5180bde7cc34",
                    parent_name="child2",
                    last_modified="2019-02-12",
                )

            if folder_id == "f60b8a3d-5496-463d-bd15-5180bde7cc34":
                return Folder(
                    id=14142,
                    uuid="f60b8a3d-5496-463d-bd15-5180bde7cc34",
                    name="child2",
                    parent_uuid="08795992-34f5-11e9-8506-d31757930e95",
                    parent_name="child1",
                    last_modified="2019-02-12",
                )
            if folder_id == "08795992-34f5-11e9-8506-d31757930e95":
                return Folder(
                    id=1234,
                    uuid="08795992-34f5-11e9-8506-d31757930e95",
                    name="child1",
                    parent_uuid=None,
                    parent_name=None,
                    last_modified="2019-02-12",
                )

        mock_cqrs.get_folder_details.side_effect = folder_details_side_effect

        contents = views.get_folder_contents(request)
        assert contents["links"] == {
            "grandparent": {
                "href": "/api/v2/admin/catalog/get_folder_contents",
                "name": None,
                "id": None,
            },
            "parent": {
                "href": "/api/v2/admin/catalog/get_folder_contents?folder_id=08795992-34f5-11e9-8506-d31757930e95",
                "name": "child1",
                "id": "08795992-34f5-11e9-8506-d31757930e95",
            },
            "self": {
                "href": "/api/v2/admin/catalog/get_folder_contents?folder_id=f60b8a3d-5496-463d-bd15-5180bde7cc34",
                "name": "child2",
                "id": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
            },
        }

        request.params = {"folder_id": "8a22f984-351e-11e9-8df4-afc0e680fd64"}
        request.current_route = "/api/v2/admin/catalog/get_folder_contents?folder_id=8a22f984-351e-11e9-8df4-afc0e680fd64"
        contents2 = views.get_folder_contents(request)

        assert contents2["links"] == {
            "grandparent": {
                "href": "/api/v2/admin/catalog/get_folder_contents?folder_id=08795992-34f5-11e9-8506-d31757930e95",
                "name": "child1",
                "id": "08795992-34f5-11e9-8506-d31757930e95",
            },
            "parent": {
                "href": "/api/v2/admin/catalog/get_folder_contents?folder_id=f60b8a3d-5496-463d-bd15-5180bde7cc34",
                "name": "child2",
                "id": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
            },
            "self": {
                "href": "/api/v2/admin/catalog/get_folder_contents?folder_id=8a22f984-351e-11e9-8df4-afc0e680fd64",
                "name": "child3",
                "id": "8a22f984-351e-11e9-8df4-afc0e680fd64",
            },
        }

    def test_get_folder_contents_forbidden(self):
        """Hello World"""
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": false} }
                    """
                }
            },
            params={},
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )

        with pytest.raises(HTTPForbidden):
            views.get_folder_contents(request)

    def test_get_folder_contents_notfound(self):
        """Hello World"""
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )

        def not_found(uuid, page, page_size):
            raise KeyError

        request.query_instances[
            "zsnl_domains.admin.catalog"
        ].get_folder_contents = not_found

        with pytest.raises(HTTPNotFound):
            views.get_folder_contents(request)

    def test_get_folder_content_page_page_size_errors(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={
                "folder_id": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                "page": "a",
                "page_size": "a",
            },
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )

        with pytest.raises(HTTPBadRequest):
            views.get_folder_contents(request)

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={
                "folder_id": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                "page": "1",
                "page_size": "a",
            },
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )
        with pytest.raises(HTTPBadRequest):
            views.get_folder_contents(request)

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={
                "folder_id": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                "page": "0",
                "page_size": "10",
            },
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )
        with pytest.raises(HTTPBadRequest):
            views.get_folder_contents(request)

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={
                "folder_id": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                "page": "1",
                "page_size": "0",
            },
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )
        with pytest.raises(HTTPBadRequest):
            views.get_folder_contents(request)

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={
                "folder_id": "f60b8a3d-5496-463d-bd15-5180bde7cc34",
                "page": "1",
                "page_size": "1500",
            },
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )
        with pytest.raises(HTTPBadRequest):
            views.get_folder_contents(request)

    def test_search_catalog_forbidden(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": false} }
                    """
                }
            },
            params={},
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )

        with pytest.raises(HTTPForbidden):
            views.search_catalog(request)

    def test_search_catalog_notfound(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )

        def not_found(keyword, type, page, page_size):
            raise KeyError

        request.query_instances[
            "zsnl_domains.admin.catalog"
        ].search_catalog = not_found

        with pytest.raises(HTTPNotFound):
            views.search_catalog(request)

    def test_search_catalog_by_type(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={
                "type": "case_type",
                "keyword": "test zaak_type",
                "page": 1,
                "page_size": 10,
            },
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )

        request.query_instances[
            "zsnl_domains.admin.catalog"
        ].search_catalog.return_value = [
            FolderEntry(
                id=123,
                uuid="d234a001-d664-4c91-9048-6bbe78330a3c",
                name="test zaak_type",
                entry_type="case_type",
                active=True,
            ),
            FolderEntry(
                id=1235,
                uuid="34cdcba6-b8e0-4cbc-8800-fb149ebdf949",
                name="test zaak_type copy",
                entry_type="case_type",
                active=True,
            ),
        ]

        contents = views.search_catalog(request)
        assert contents == {
            "data": [
                {
                    "type": "folder_entry",
                    "id": "d234a001-d664-4c91-9048-6bbe78330a3c",
                    "attributes": {
                        "type": "case_type",
                        "name": "test zaak_type",
                        "active": True,
                    },
                    "links": {
                        "self": "/api/v2/admin/catalog/get_entry_detail?type=case_type&item_id=d234a001-d664-4c91-9048-6bbe78330a3c"
                    },
                },
                {
                    "type": "folder_entry",
                    "id": "34cdcba6-b8e0-4cbc-8800-fb149ebdf949",
                    "attributes": {
                        "type": "case_type",
                        "name": "test zaak_type copy",
                        "active": True,
                    },
                    "links": {
                        "self": "/api/v2/admin/catalog/get_entry_detail?type=case_type&item_id=34cdcba6-b8e0-4cbc-8800-fb149ebdf949"
                    },
                },
            ],
            "links": {"self": {"href": "mock_current_route"}},
        }

    def test_get_entry_detail_error(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"item_id": uuid4(), "type": "nonexistent"},
            query_instances={},
        )

        with pytest.raises(HTTPBadRequest):
            views.get_entry_detail(request)

    def test_get_entry_detail_error2(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"type": "case_type"},
            query_instances={},
        )

        with pytest.raises(HTTPBadRequest):
            views.get_entry_detail(request)

    def test_get_entry_detail(self):
        mock_query_instance = mock.MagicMock()
        mock_query_instance.get_folder_details().name = "foo"
        mock_query_instance.get_folder_details().uuid = str(uuid4())
        mock_query_instance.get_folder_details().parent_name = "foo"
        mock_query_instance.get_folder_details().parent_uuid = str(uuid4())
        mock_query_instance.get_folder_details().last_modified = None

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"item_id": str(uuid4()), "type": "folder"},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
        )

        rv = views.get_entry_detail(request)
        assert rv == {
            "data": {
                "type": "folder",
                "id": mock_query_instance.get_folder_details().uuid,
                "attributes": {
                    "name": mock_query_instance.get_folder_details().name,
                    "parent_name": mock_query_instance.get_folder_details().parent_name,
                    "parent_id": mock_query_instance.get_folder_details().parent_uuid,
                    "last_modified": mock_query_instance.get_folder_details().last_modified,
                },
                "links": {
                    "self": f"/api/v2/admin/catalog/get_folder_contents?folder_id={mock_query_instance.get_folder_details().uuid}"
                },
            },
            "links": {"self": "mock_current_route"},
        }

    def test_change_case_type_online_status(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )
        request.body = {
            "case_type_uuid": "5c713690-5161-11e9-8850-5be9d1896f64",
            "active": True,
            "reason": "test_reason",
        }
        rv = views.change_case_type_online_status(request)

        assert rv == {"data": {"success": True}}

    def test_change_case_type_online_status_no_admin(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )

        with pytest.raises(HTTPForbidden):
            views.change_case_type_online_status(request)

    def test_change_case_type_online_status_incorrect_params(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )

        with pytest.raises(HTTPBadRequest):
            request.body = {
                "case_type_uuid": "5c713690-5161-11e9-8850-5be9d1896f64",
                # "active": True,
            }
            views.change_case_type_online_status(request)

        with pytest.raises(HTTPBadRequest):
            request.body = {
                # "case_type_uuid": "5c713690-5161-11e9-8850-5be9d1896f64",
                "active": True
            }
            views.change_case_type_online_status(request)

    def test_move_folder_entries(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )
        request.body = {
            "folder_entries": [],
            "destination_folder_id": str(uuid4()),
        }
        rv = views.move_folder_entries(request)

        assert rv == {"data": {"success": True}}

    def test_move_folder_entries_no_admin(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )

        with pytest.raises(HTTPForbidden):
            views.move_folder_entries(request)

    def test_move_folder_entries_incorrect_params(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )

        with pytest.raises(HTTPBadRequest):
            request.body = {
                "folder_entries": []
                # "destination_folder_id"
            }
            views.move_folder_entries(request)

        with pytest.raises(HTTPBadRequest):
            request.body = {
                # "folder_entries": []
                "destination_folder_id": str(uuid4())
            }
            views.move_folder_entries(request)

    def test_get_attribute_detail_no_admin(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
        )

        with pytest.raises(HTTPForbidden):
            views.get_attribute_detail(request)

    def test_get_attribute_detail_incorrect_params(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
        )

        with pytest.raises(HTTPBadRequest):
            views.get_attribute_detail(request)

    def test_get_attribute_detail(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"attribute_id": "0d0280b5-065f-4756-a3c4-69fd026b05a4"},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
            current_route="/api/v2/admin/catalog/get_attribute_detail?attribute_id=0d0280b5-065f-4756-a3c4-69fd026b05a4",
        )
        mock_query_instance.get_attribute_details_for_edit.return_value = (
            Attribute(
                uuid="0d0280b5-065f-4756-a3c4-69fd026b05a4",
                id=2,
                attribute_type="file",
                name="name",
                public_name="public_name",
                magic_string="magic_string",
                type_multiple=False,
                value_default="value_default",
                help="help",
                sensitive_field=True,
                document_origin="origin",
                document_trust_level="trust_level",
                document_category="document_category",
                category_uuid="0d0280b5-065f-4756-a3c4-69fd026b05a6",
                category_name="category_name",
                attribute_values=[],
                appointment_location_id=None,
                appointment_product_id=None,
                appointment_interface_uuid=None,
                relationship_type=None,
                relationship_name=None,
                relationship_uuid=None,
                commit_message=None,
            )
        )

        res = views.get_attribute_detail(request)

        assert res == {
            "data": {
                "type": "attribute",
                "id": "0d0280b5-065f-4756-a3c4-69fd026b05a4",
                "attributes": {
                    "attribute_type": "file",
                    "name": "name",
                    "public_name": "public_name",
                    "magic_string": "magic_string",
                    "sensitive_field": True,
                    "type_multiple": False,
                    "help": "help",
                    "value_default": "value_default",
                    "category_uuid": "0d0280b5-065f-4756-a3c4-69fd026b05a6",
                    "category_name": "category_name",
                    "document_origin": "origin",
                    "document_trust_level": "trust_level",
                    "document_category": "document_category",
                    "attribute_values": [],
                    "appointment_location_id": None,
                    "appointment_product_id": None,
                    "appointment_interface_uuid": None,
                    "relationship_data": {
                        "type": None,
                        "name": None,
                        "uuid": None,
                    },
                },
            },
            "links": {
                "self": "/api/v2/admin/catalog/get_attribute_detail?attribute_id=0d0280b5-065f-4756-a3c4-69fd026b05a4"
            },
        }

    def test_edit_attribute(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )
        request.body = {
            "attribute_uuid": str(uuid4()),
            "fields": {"public_name": "public_name"},
        }
        rv = views.edit_attribute(request)

        assert rv == {"data": {"success": True}}

    def test_edit_attribute_no_admin(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )

        with pytest.raises(HTTPForbidden):
            views.edit_attribute(request)

    def test_edit_attribute_incorrect_params(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )

        with pytest.raises(HTTPBadRequest):
            request.body = {"attribute_uuid": uuid4()}
            views.edit_attribute(request)

        with pytest.raises(HTTPBadRequest):
            request.body = {"fields": {"public_name": "public_name"}}
            views.edit_attribute(request)

    def test_create_attribute(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )
        request.body = {
            "attribute_uuid": str(uuid4()),
            "fields": {"public_name": "public_name"},
        }
        rv = views.create_attribute(request)

        assert rv == {"data": {"success": True}}

    def test_create_attribute_no_admin(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )

        with pytest.raises(HTTPForbidden):
            views.create_attribute(request)

    def test_create_attribute_incorrect_params(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )

        with pytest.raises(HTTPBadRequest):
            request.body = {"attribute_uuid": uuid4()}
            views.create_attribute(request)

        with pytest.raises(HTTPBadRequest):
            request.body = {"fields": {"public_name": "public_name"}}
            views.create_attribute(request)

    def test_generate_magic_string_no_admin(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={"string_input": "doc_varibale_1"},
            command_instances={"none": None},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
        )

        with pytest.raises(HTTPForbidden):
            views.generate_magic_string(request)

    def test_genearate_magic_string_incorrect_params(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            command_instances={"none": None},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
        )

        with pytest.raises(HTTPBadRequest):
            views.generate_magic_string(request)

    def test_generate_magic_stirng(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"string_input": "doc_varibale_1"},
            command_instances={"none": None},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
        )
        mock_query_instance.generate_magic_string.return_value = (
            "doc_variable_2"
        )
        rv = views.generate_magic_string(request)

        assert rv == {
            "data": {"magic_string": "doc_variable_2"},
            "links": {"self": "mock_current_route"},
        }

    def test_views_no_admin(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )

        admin_views = [
            views.rename_folder,
            views.move_folder_entries,
            views.create_folder,
            views.change_case_type_online_status,
            views.update_case_type_version,
            # views.get_folder_contents,
        ]
        for handler in admin_views:
            with pytest.raises(HTTPForbidden):
                handler(request)

    def test_rename_folder(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )
        request.body = {"name": "new_folder_name", "folder_uuid": str(uuid4())}
        views.rename_folder(request)

        with pytest.raises(HTTPBadRequest):
            request.body = {
                # "name": "new_folder_name"
                "folder_uuid": str(uuid4())
            }
            views.rename_folder(request)

    def test_create_folder(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )
        request.body = {
            "folder_uuid": str(uuid4()),
            "parent_uuid": str(uuid4()),
            "name": "new folder name",
        }
        views.create_folder(request)
        with pytest.raises(HTTPBadRequest):
            request.body = {
                # "name": "new_folder_name"
                "parent_uuid": None,
                "folder_uuid": str(uuid4()),
            }
            views.create_folder(request)

    def test_get_case_type_history(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                      {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                      """
                }
            },
            params={},
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )

        case_type_id_1 = uuid4()
        case_type_id_2 = uuid4()
        request.query_instances[
            "zsnl_domains.admin.catalog"
        ].get_case_type_history.return_value = [
            CaseTypeVersion(
                uuid="1",
                case_type_uuid=str(case_type_id_1),
                reason="Test case type version",
                name="case_type",
                username="case_type",
                display_name="case_type",
                active=True,
                version=1,
                last_modified="",
                created="",
                change_note="test",
                modified_components={
                    "basic_properties": True,
                    "case_actions": False,
                    "phases_and_assignment": True,
                    "custom_fields": False,
                },
            ),
            CaseTypeVersion(
                uuid="2",
                case_type_uuid=str(case_type_id_2),
                reason="Test case type version",
                name="case_type",
                username="case_type",
                display_name="case_type",
                active=False,
                version=2,
                last_modified=None,
                created=None,
                change_note="test",
                modified_components={
                    "basic_properties": True,
                    "case_actions": False,
                    "phases_and_assignment": True,
                    "custom_fields": False,
                },
            ),
        ]

        contents = views.get_case_type_history(request)
        assert contents == {
            "data": [
                {
                    "type": "case_type_version",
                    "case_type_id": str(case_type_id_1),
                    "attributes": {
                        "id": "1",
                        "name": "case_type",
                        "username": "case_type",
                        "display_name": "case_type",
                        "created": None,
                        "last_modified": None,
                        "active": True,
                        "reason": "Test case type version",
                        "version": 1,
                        "change_note": "test",
                        "modified_components": {
                            "basic_properties": True,
                            "case_actions": False,
                            "phases_and_assignment": True,
                            "custom_fields": False,
                        },
                    },
                },
                {
                    "type": "case_type_version",
                    "case_type_id": str(case_type_id_2),
                    "attributes": {
                        "id": "2",
                        "name": "case_type",
                        "username": "case_type",
                        "display_name": "case_type",
                        "created": None,
                        "last_modified": None,
                        "active": False,
                        "reason": "Test case type version",
                        "version": 2,
                        "change_note": "test",
                        "modified_components": {
                            "basic_properties": True,
                            "case_actions": False,
                            "phases_and_assignment": True,
                            "custom_fields": False,
                        },
                    },
                },
            ],
            "links": {"self": {"href": "mock_current_route"}},
        }

    def test_get_case_type_history_forbidden(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": false} }
                    """
                }
            },
            params={},
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )

        with pytest.raises(HTTPForbidden):
            views.get_case_type_history(request)

    def test_get_case_type_history_notfound(self):
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"zsnl_domains.admin.catalog": mock.MagicMock()},
        )

        def not_found(keyword):
            raise KeyError

        request.query_instances[
            "zsnl_domains.admin.catalog"
        ].get_case_type_history = not_found

        with pytest.raises(HTTPNotFound):
            views.get_case_type_history(request)

    def test_set_active_case_type_version_bad_request(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )
        request.body = {"_id": str(uuid4()), "reason": "test reason"}

        with pytest.raises(HTTPBadRequest):
            views.update_case_type_version(request)

    def test_set_active_case_type_version(self):
        mock_command_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={"none": None},
            command_instances={
                "zsnl_domains.admin.catalog": mock_command_instance
            },
        )
        request.body = {
            "case_type_id": str(uuid4()),
            "version_id": str(uuid4()),
            "reason": "test reason",
        }
        rv = views.update_case_type_version(request)

        assert rv == {"data": {"success": True}}
