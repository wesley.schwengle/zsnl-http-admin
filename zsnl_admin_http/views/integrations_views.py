# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import serializers
from minty.cqrs import UserInfo
from minty_pyramid.session_manager import protected_route
from pyramid.request import Request


@protected_route("beheer_zaaktype_admin")
def get_active_appointment_integrations(
    request: Request, user_info: UserInfo
) -> dict:
    """Get active appointment integrations."""
    qry = request.get_query_instance(
        domain="zsnl_domains.admin.integrations", user_uuid=user_info.user_uuid
    )

    integrations = qry.get_active_appointment_integrations()

    return {
        "data": [
            serializers.integration(integration)
            for integration in integrations
        ],
        "links": {"self": f"{request.current_route_path()}"},
    }


@protected_route("beheer_zaaktype_admin")
def get_active_appointment_v2_integrations(
    request: Request, user_info: UserInfo
) -> dict:
    """Get active apoointment integrations."""
    qry = request.get_query_instance(
        domain="zsnl_domains.admin.integrations", user_uuid=user_info.user_uuid
    )

    integrations = qry.get_active_appointment_v2_integrations()

    return {
        "data": [
            serializers.integration(integration)
            for integration in integrations
        ],
        "links": {"self": f"{request.current_route_path()}"},
    }


@protected_route("beheer_zaaktype_admin")
def get_active_document_integrations(request: Request, user_info):
    """Get the list of active document integrations."""
    qry = request.get_query_instance(
        domain="zsnl_domains.admin.integrations", user_uuid=user_info.user_uuid
    )

    integrations = qry.get_active_integrations_for_document()

    return {
        "data": [
            serializers.integration(integration)
            for integration in integrations
        ],
        "links": {"self": f"{request.current_route_path()}"},
    }
