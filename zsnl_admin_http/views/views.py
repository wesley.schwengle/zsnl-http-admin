# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from . import serializers
from minty_pyramid.session_manager import protected_route
from pyramid.httpexceptions import HTTPBadRequest, HTTPNotFound
from pyramid.request import Request


@protected_route("beheer_zaaktype_admin")
def get_folder_contents(request: Request, user_info):
    """Get folder contents for specified `folder_id`.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """

    folder_id = request.params.get("folder_id")
    raw_page = request.params.get("page", 1)
    raw_page_size = request.params.get("page_size", 50)
    raw_page, raw_page_size = _check_paging_limits(
        page=raw_page, page_size=raw_page_size, max_results=50
    )
    root_folder = True if folder_id is None else False

    query_instance = request.get_query_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )

    try:
        folder_contents = query_instance.get_folder_contents(
            folder_id, raw_page, raw_page_size
        )
    except KeyError:
        raise HTTPNotFound(json={"error": f"Folder not found: '{folder_id}'"})
    if not root_folder:
        folder_details = query_instance.get_folder_details(folder_id)

    folder = {
        "href": f"{request.current_route_path()}",
        "name": None,
        "id": None,
    }
    parent = {"href": None, "name": None, "id": None}
    grandparent = {"href": None, "name": None, "id": None}

    if root_folder is True:
        folder["href"] = f"{request.current_route_path()}"
        parent = None
        grandparent = None
    elif folder_details.parent_name is None:
        folder["href"] = f"{request.current_route_path()}"
        folder["name"] = folder_details.name
        folder["id"] = f"{folder_details.uuid}"

        parent["href"] = request.route_path("get_folder_contents")
        grandparent = None
    else:
        folder["href"] = f"{request.current_route_path()}"
        folder["name"] = folder_details.name
        folder["id"] = f"{folder_details.uuid}"

        parent["href"] = request.route_path(
            "get_folder_contents",
            _query={"folder_id": f"{folder_details.parent_uuid}"},
        )
        parent["name"] = f"{folder_details.parent_name}"
        parent["id"] = f"{folder_details.parent_uuid}"

        parent_details = query_instance.get_folder_details(
            f"{folder_details.parent_uuid}"
        )

        if parent_details.parent_name is None:
            grandparent["href"] = request.route_path("get_folder_contents")
        else:
            grandparent["href"] = request.route_path(
                "get_folder_contents",
                _query={"folder_id": f"{parent_details.parent_uuid}"},
            )
            grandparent["name"] = f"{parent_details.parent_name}"
            grandparent["id"] = f"{parent_details.parent_uuid}"

    links = {"self": folder, "parent": parent, "grandparent": grandparent}

    request.response.headerlist.extend((("Cache-Control", "no-store"),))

    return {
        "data": [serializers.folder_entry(entry) for entry in folder_contents],
        "links": links,
    }


@protected_route(
    "beheer_zaaktype_admin",
    "documenten_intake_all",
    "documenten_intake_subject",
)
def search_catalog(request: Request, user_info):
    """Search catalog with keyword.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """

    keyword = request.params.get("keyword")
    filter_type = request.params.get("filter[type]")
    raw_page = request.params.get("page", 1)
    raw_page_size = request.params.get("page_size", 50)

    raw_page, raw_page_size = _check_paging_limits(
        page=raw_page, page_size=raw_page_size, max_results=50
    )

    query_instance = request.get_query_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )

    try:
        catalog_result = query_instance.search_catalog(
            keyword=keyword,
            type=filter_type,
            page=raw_page,
            page_size=raw_page_size,
        )
    except KeyError:
        raise HTTPNotFound(json={"error": f"No result found: '{keyword}'"})

    search_link = {"self": {"href": f"{request.current_route_path()}"}}

    return {
        "data": [serializers.folder_entry(entry) for entry in catalog_result],
        "links": search_link,
    }


entry_type_method_map = {
    "folder": "get_folder_details",
    "case_type": "get_case_type_details",
    "object_type": "get_object_type_details",
    "attribute": "get_attribute_details",
    "email_template": "get_email_template_details",
    "document_template": "get_document_template_details",
    "custom_object_type": "get_custom_object_type_details",
}

entry_type_serializer_map = {
    "folder": serializers.folder_details,
    "case_type": serializers.case_type_details,
    "object_type": serializers.object_type_details,
    "attribute": serializers.attribute_details,
    "email_template": serializers.email_template_details,
    "document_template": serializers.document_template_details,
    "custom_object_type": serializers.custom_object_type_details,
}


@protected_route("beheer_zaaktype_admin")
def get_entry_detail(request: Request, user_info):
    try:
        entry_id = request.params["item_id"]
        entry_type = request.params["type"]
    except KeyError as e:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": f"Required parameter not specified: '{e}'"}
                ]
            }
        ) from e

    try:
        method = entry_type_method_map[entry_type]
    except KeyError as e:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Unknown item type: '{entry_type}'"}]}
        ) from e

    query_instance = request.get_query_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )

    details = getattr(query_instance, method)(entry_id)

    request.response.headerlist.extend((("Cache-Control", "no-store"),))

    return {
        "data": entry_type_serializer_map[entry_type](details, user_info),
        "links": {"self": f"{request.current_route_path()}"},
    }


@protected_route("beheer_zaaktype_admin")
def change_case_type_online_status(request: Request, user_info):
    """Set case type status to active/inactive.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPForbidden: forbidden
    :raises HTTPBadRequest: bad request
    :return: response
    :rtype: dict
    """
    try:
        casetype_uuid = request.json_body["case_type_uuid"]
        active = request.json_body["active"]
        reason = request.json_body["reason"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        )

    command_params = {
        "case_type_uuid": casetype_uuid,
        "active": active,
        "reason": reason,
    }

    cmd = request.get_command_instance(
        domain="zsnl_domains.admin.catalog", user_uuid=user_info.user_uuid
    )

    cmd.change_case_type_online_status(**command_params)

    return {"data": {"success": True}}


@protected_route("beheer_zaaktype_admin")
def move_folder_entries(request: Request, user_info):
    """Move folder entries in the catalog to another folder.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPForbidden: forbidden
    :raises HTTPBadRequest: bad request
    :return: response
    :rtype: dict
    """
    try:
        folder_entries = request.json_body["folder_entries"]
        destination_folder_id = request.json_body["destination_folder_id"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        )

    cmd = request.get_command_instance(
        domain="zsnl_domains.admin.catalog", user_uuid=user_info.user_uuid
    )

    cmd.move_folder_entries(
        folder_entries=folder_entries,
        destination_folder_id=destination_folder_id,
    )

    return {"data": {"success": True}}


@protected_route("beheer_zaaktype_admin")
def get_attribute_detail(request: Request, user_info):
    """Get detailed information about an attribute.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPForbidden: forbidden
    :raises HTTPBadRequest: bad request
    :return: response
    :rtype: dict
    """
    try:
        attribute_uuid = request.params["attribute_id"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        )

    qry = request.get_query_instance(
        domain="zsnl_domains.admin.catalog", user_uuid=user_info.user_uuid
    )

    details = qry.get_attribute_details_for_edit(uuid=attribute_uuid)

    return {
        "data": serializers.attribute_details_for_edit(details),
        "links": {"self": f"{request.current_route_path()}"},
    }


@protected_route("beheer_zaaktype_admin")
def edit_attribute(request: Request, user_info):
    """Edit/update an attribute.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPForbidden: forbidden
    :raises HTTPBadRequest: bad request
    :return: response
    :rtype: dict
    """
    try:
        attribute_uuid = request.json_body["attribute_uuid"]
        fields = request.json_body["fields"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        )

    command_params = {"attribute_uuid": attribute_uuid, "fields": fields}

    cmd = request.get_command_instance(
        domain="zsnl_domains.admin.catalog", user_uuid=user_info.user_uuid
    )

    cmd.edit_attribute(**command_params)

    return {"data": {"success": True}}


@protected_route("beheer_zaaktype_admin")
def create_attribute(request: Request, user_info):
    """Create an attribute.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPForbidden: forbidden
    :raises HTTPBadRequest: bad request
    :return: response
    :rtype: dict
    """
    try:
        attribute_uuid = request.json_body["attribute_uuid"]
        fields = request.json_body["fields"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        )

    command_params = {"attribute_uuid": attribute_uuid, "fields": fields}

    cmd = request.get_command_instance(
        domain="zsnl_domains.admin.catalog", user_uuid=user_info.user_uuid
    )

    cmd.create_attribute(**command_params)

    return {"data": {"success": True}}


@protected_route("beheer_zaaktype_admin")
def generate_magic_string(request: Request, user_info):
    """Generate magic string from user inputted string.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPForbidden: forbidden
    :raises HTTPBadRequest: bad request
    :return: response
    :rtype: dict
    """
    try:
        string_input = request.params["string_input"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        )

    qry = request.get_query_instance(
        domain="zsnl_domains.admin.catalog", user_uuid=user_info.user_uuid
    )

    magic_string = qry.generate_magic_string(string_input=string_input)

    return {
        "data": {"magic_string": magic_string},
        "links": {"self": f"{request.current_route_path()}"},
    }


@protected_route("beheer_zaaktype_admin")
def rename_folder(request: Request, user_info):
    try:
        folder_uuid = request.json_body["folder_uuid"]
        folder_name = request.json_body["name"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        )

    cmd = request.get_command_instance(
        domain="zsnl_domains.admin.catalog", user_uuid=user_info.user_uuid
    )

    cmd.rename_folder(folder_uuid=folder_uuid, name=folder_name)

    return {"data": {"success": True}}


@protected_route("beheer_zaaktype_admin")
def create_folder(request: Request, user_info):
    try:
        folder_uuid = request.json_body["folder_uuid"]
        folder_name = request.json_body["name"]
        parent_uuid = request.json_body["parent_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        )

    cmd = request.get_command_instance(
        domain="zsnl_domains.admin.catalog", user_uuid=user_info.user_uuid
    )

    cmd.create_folder(
        folder_uuid=folder_uuid, parent_uuid=parent_uuid, name=folder_name
    )

    return {"data": {"success": True}}


@protected_route("beheer_zaaktype_admin")
def get_case_type_history(request: Request, user_info):
    """Get case type version history.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """
    case_type_id = request.params.get("case_type_id")
    query_instance = request.get_query_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )

    try:
        case_type_version_history = query_instance.get_case_type_history(
            case_type_id
        )
    except KeyError:
        raise HTTPNotFound(
            json={"error": f"No result found for: '{case_type_id}'"}
        )

    search_link = {"self": {"href": f"{request.current_route_path()}"}}

    return {
        "data": [
            serializers.case_type_version_history_details(entry)
            for entry in case_type_version_history
        ],
        "links": search_link,
    }


@protected_route("beheer_zaaktype_admin")
def update_case_type_version(request: Request, user_info):
    """Activate a version on a case type.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """
    try:
        case_type_uuid = request.json_body["case_type_id"]
        case_type_version_uuid = request.json_body["version_id"]
        reason = request.json_body["reason"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        )

    cmd = request.get_command_instance(
        domain="zsnl_domains.admin.catalog", user_uuid=user_info.user_uuid
    )

    cmd.update_case_type_version(
        case_type_uuid=case_type_uuid,
        version_uuid=case_type_version_uuid,
        reason=reason,
    )

    return {"data": {"success": True}}


def _check_paging_limits(page, page_size, max_results):
    try:
        page = int(page)
    except ValueError as e:
        raise HTTPBadRequest(
            json={
                "errors": [{"title": "Parameter page is not a valid integer"}]
            }
        ) from e

    try:
        page_size = int(page_size)
    except ValueError as e:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": "Parameter page_size is not a valid integer"}
                ]
            }
        ) from e

    if page < 1:
        raise HTTPBadRequest(
            json={"errors": [{"title": "'page' must be >= 1"}]}
        )

    if page_size < 1:
        raise HTTPBadRequest(
            json={"errors": [{"title": "'page_size' must be >= 1"}]}
        )

    if page_size > max_results:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {
                        "title": f"Requested page size {page_size} > max size {max_results}."
                    }
                ]
            }
        )

    return int(page), int(page_size)
